#! /usr/bin/env python
"""
Contains the various fixed parameters to use for the analysis
=============================================================


"""
# Created: Mon Feb 22, 2016  03:38PM
# Last modified: Thu Jan 11, 2018  03:00PM
# Copyright: Bedartha Goswami <goswami@pik-potsdam.de>

# initialize parameters for stdout
VERBOSE = True
PROGBAR = True

# initalize parameters for the analysis
SF = 2              # sampling frequency
LD = 0.30           # link density
WS = 100            # window size for transition analysis
ST = 10             # step size by which the window is moved
NS = 1000           # number of suggogate networks to generate
ALPHA = 0.01        # significance level of statistical test




