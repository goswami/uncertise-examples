#! /usr/bin/env python
"""
Generate the time series for the synthetic example
==================================================

Reference
=========

B. Goswami, N. Boers, A. Rheinwalt, N. Marwan, J. Heitzig, S. F. M.
Breitenbach, J. Kurths
Abrupt transitions in time series with uncertainties,
Nature Communications 9 (2018)
doi:10.1038/s41467-017-02456-6

"""
# Created: Mon Feb 22, 2016  03:38PM
# Last modified: Thu Jan 11, 2018  03:01PM
# Copyright: Bedartha Goswami <goswami@pik-potsdam.de>


import numpy as np
from scipy.stats import norm
from scipy.stats import gaussian_kde

from uncertise.utils import _printmsg
from uncertise.utils import _progressbar_start
from uncertise.utils import _progressbar_update
from uncertise.utils import _progressbar_finish

from params import SF


if __name__ == "__main__":
    # set stdout parameters
    VERBOSE = True
    PROGBAR = True

    # set parameters for analysis
    _printmsg("Set up synthetic example...", VERBOSE)
    T = 50                              # time period of sine curve
    A = 1.                              # amplitude of sine curve
    nt = 1000                           # total time range
    tsplit = 675                        # time when the means split up
    sample_res = SF                     # sampling frequency

    # get basic dynamical variables
    t = np.arange(nt)
    x_ = A * np.sin((2 * np.pi / T) * t) 
    I1 = np.r_[  # impulse causing jumps in dynamics
               np.zeros(200),
               5. * np.ones(200),
               np.linspace(5., 0., 50),
               np.zeros(550)
               ]
    # split to bimodality
    x_ = x_ + I1
    x1 = np.r_[
                x_[:tsplit],
                10.00 * x_[tsplit:]
                ]
    x2 = np.r_[
                x_[:tsplit],
                -10.00 * x_[tsplit:]
                ]

    # sample the time series with sampling noise
    ts = t[::sample_res]              # sample every 'sample_res'th element
    ns = len(ts)
    es = 3.00 # dynamic noise
    x1s = x1[::sample_res] + es * np.random.randn(ns)
    x2s = x2[::sample_res] + es * np.random.randn(ns)

    # create matrix to store series of PDFs
    _printmsg("Creating probability distribution matrix...", VERBOSE)
    lo1 = x1.min() - 5. * es
    hi1 = x1.max() + 5. * es
    lo2 = x2.min() - 5. * es
    hi2 = x2.max() + 5. * es
    lo = min(lo1, lo2)
    hi = min(hi1, hi2)
    var_res = 1000
    var_span = np.linspace(lo, hi, var_res)
    pdfmat = np.zeros((ns, var_res))
    prog_bar = _progressbar_start(ns, PROGBAR)
    ngrid = 1000
    timeseries_grid = np.zeros((ngrid, nt))
    ss = 1.50 # sampling noise
    for i in range(ns):
        pdf1 = norm(loc=x1s[i], scale=ss)
        pdf2 = norm(loc=x2s[i], scale=ss)
        samp = np.r_[
                    pdf1.rvs(int(ngrid / 2)),
                    pdf2.rvs(int(ngrid / 2))
                ]
        timeseries_grid[:, i] = samp
        kde = gaussian_kde(samp)
        pdfmat[i] = kde(var_span)
        _progressbar_update(prog_bar, i + 1)
    _progressbar_finish(prog_bar)

    # save output
    FOUT = "pdfseries"
    np.savez(FOUT,
             pdfmat=pdfmat, ts=ts, var_span=var_span
             )
    _printmsg("saved to: %s.npz" % FOUT, VERBOSE)
