#! /usr/bin/env python
"""
Synthetic example for abrupt transitions in time series with uncertainties
==========================================================================

Reference
=========

B. Goswami, N. Boers, A. Rheinwalt, N. Marwan, J. Heitzig, S. F. M.
Breitenbach, J. Kurths
Abrupt transitions in time series with uncertainties,
Nature Communications 9 (2018)
doi:10.1038/s41467-017-02456-6

"""
# Created: Mon Feb 22, 2016  03:38PM
# Last modified: Thu Jan 11, 2018  02:24PM
# Copyright: Bedartha Goswami <goswami@pik-potsdam.de>


import numpy as np

from uncertise.utils import _printmsg
from uncertise import distributions
from uncertise import networks

from params import VERBOSE
from params import PROGBAR
from params import LD


if __name__ == "__main__":
    # load the data 
    DAT = np.load("pdfseries.npz")
    pdfmat = DAT["pdfmat"]
    var_span = DAT["var_span"]

    # get the series of CDF from the PDFs
    cdfmat, iqr = distributions.cumulative(pdfmat, var_span, VERBOSE, PROGBAR)

    # get the recurrence network as in igraph.Graph object
    G = networks.prob_recurrence_network(cdfmat, var_span, LD, iqr, 
                                         (1E-1, 5E-1), 5E-4,
                                         VERBOSE, PROGBAR)
    # note there will be a warning message if the required link density is not
    # bracketed by the thresholds given by the function parameter 'thr_lims'.
    # However, the code will not exit, but only return an empty graph G.
    # Therefore, it is useful to have a check at this point
    if G.density() == 0.:
        import sys
        print("Please increase the threshold limits!")
        sys.exit()
    
    # save output
    FOUT = "./recurrencenetwork.igraph"
    G.write_pickle(FOUT)
    _printmsg("saved to: %s" % FOUT, VERBOSE)
