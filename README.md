Example script for a synthetic example
======================================

General noes
------------

This repo contains a few scripts that generate the synthetic example used in
our paper [1] and thereafter uses the probability density function (PDF) time
series to estimate the artifically imposed abrupt transitions in the data.

Usage 
------

In order to use this repo, it is advised that you have the 'uncertise' package
in your Python path (it suffices to put the directory of the 'uncertise' [2]
package in your current working directory, where the scripts are also located).
Then, run the the following commands in the given order:

    $ python pdfseries.py
    $ python get_recurrencenetwork.py
    $ python get_transitions.py
    $ python plot_results.py

At each step above, the scripts print messages ont he screen, telling you what
is going on, and in the end, some kind of output file is saved to disk in the
current working directory. In the last case, the output is a PNG figure,
equivalent to the left column of Fig. 2 of the paper.

References
----------

[1] B. Goswami, N. Boers, A. Rheinwalt, N. Marwan, J. Heitzig, S. F. M.
        Breitenbach, J. Kurths
    Abrupt transitions in time series with uncertainties,
    Nature Communications 9 (2018)
    doi:10.1038/s41467-017-02456-6

[2] https://gitlab.pik-potsdam.de/goswami/uncertise

Author
------

Bedartha Goswami 

Contact
-------
goswami<at>pik-potsdam.de

License
-------

This work is licensed under a Creative Commons
Attribution-NonCommercial-NoDerivatives 4.0 International Public License. For
details please read LICENSE.txt.

Please respect the copyrights! The content is protected by the Creative Commons
License. If you use the provided programmes, text or figures, you have to refer
to the given publications and this web site (tocsy.pik-potsdam.de) as well. 

