#! /usr/bin/env python
"""
Synthetic example for abrupt transitions in time series with uncertainties
==========================================================================

Reference
=========

B. Goswami, N. Boers, A. Rheinwalt, N. Marwan, J. Heitzig, S. F. M.
Breitenbach, J. Kurths
Abrupt transitions in time series with uncertainties,
Nature Communications 9 (2018)
doi:10.1038/s41467-017-02456-6

"""
# Created: Mon Feb 22, 2016  03:38PM
# Last modified: Thu Jan 11, 2018  02:27PM
# Copyright: Bedartha Goswami <goswami@pik-potsdam.de>


import numpy as np
import igraph as ig

from uncertise.utils import _printmsg
from uncertise import events

from params import VERBOSE
from params import PROGBAR
from params import WS
from params import ST
from params import NS
from params import ALPHA

if __name__ == "__main__":
    _printmsg("load data ...", VERBOSE)
    # load the recurrence network stored as an igraph.Graph object
    FNAME = "recurrencenetwork.igraph"
    G = ig.Graph.Read_Pickle(FNAME)

    # load time scale data
    FNAME = "pdfseries.npz"
    D = np.load(FNAME)
    t = D["ts"]

    # get the community strength values for the observed data as well as the
    # degree configuration model in order to identify non-random transitions
    Q, tmid = events.community_strength_data(G, t, WS, ST, VERBOSE, PROGBAR)
    res = events.community_strength_random_model(G, t, WS, ST, NS,
                                                 VERBOSE, PROGBAR)
    Qsurr = res[0]
    tmid_surr = res[1]

    # get p-values 
    _printmsg("get p-values ...", VERBOSE)
    pvals = events.pvalues(Q, Qsurr, len(tmid), VERBOSE, PROGBAR)

    # get statistically significant windows using Holm's method
    sig_idx = events.holm(pvals, alpha=ALPHA, corr_type="dunn")

    # save output
    _printmsg("saving output ...", VERBOSE)
    FOUT = "pvalues"
    np.savez(FOUT,
             Q=Q, tmid=tmid, Qsurr=Qsurr, tmid_surr=tmid_surr,
             pvals=pvals, sig_idx=sig_idx
             )
    _printmsg("saved to: %s.npz" % FOUT, VERBOSE)
